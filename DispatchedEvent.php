<?php namespace Decoupled\Core\Event;

use Symfony\Component\EventDispatcher\Event as SymfonyEvent;

class DispatchedEvent extends SymfonyEvent implements DispatchedEventInterface{

    protected $event;

    protected $params = [];

    public function setEvent( EventInterface $event )
    {
        $this->event = $event;

        return $this;
    }

    public function getEvent()
    {
        return $this->event;
    }

    public function getParameters()
    {
        return $this->params;
    }

    public function setParameters( array $params )
    {
        $this->params = $params;

        return $this;
    }
}