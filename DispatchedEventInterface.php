<?php namespace Decoupled\Core\Event;

interface DispatchedEventInterface{

    public function setEvent( EventInterface $event );

    public function getEvent();

    public function getParameters();

    public function setParameters( array $params );
}