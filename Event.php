<?php namespace Decoupled\Core\Event;

class Event implements EventInterface{

    const DEFAULT_TYPE  = 'event.default';

    protected $type;

    protected $name;

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = (string) $name;

        return $this;
    }

    public function getType()
    {
        return $this->type ?: self::DEFAULT_TYPE;
    }

    public function setType($type)
    {
        $this->type = (string) $type;

        return $this;
    }
}