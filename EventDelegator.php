<?php namespace Decoupled\Core\Event;

class EventDelegator implements EventDelegatorInterface{

    protected $eventFactory;

    protected $dispatchers = [];

    public function when( $event )
    {
        return $this->resolveListener( $event );
    }

    public function resolveListener( $event )
    {
       $event = ( $event instanceof EventInterface ) ? $event : $this->makeEvent( $event );

       $listener = $this->makeListener( $event );

       $dispatcher = $this->getDispatcher( $event->getType() );

       $dispatcher->addListener( $listener );

       return $listener;
    }

    public function makeEvent( $event )
    {
        return $this->getEventFactory()->make( $event );
    }

    public function makeListener( EventInterface $event )
    {
        return $this->getListenerFactory()->make( $event );
    }

    public function getListenerFactory()
    {
        return $this->listenerFactory;
    }

    public function setListenerFactory( EventListenerFactoryInterface $factory )
    {
        $this->listenerFactory = $factory;

        return $this;
    }

    public function getEventFactory()
    {
        return $this->eventFactory;
    }

    public function setEventFactory( EventFactoryInterface $factory )
    {
        $this->eventFactory = $factory;

        return $this;
    }

    public function dispatch( $event, array $params = [] )
    {
        $event = ( $event instanceof EventInterface ) ? $event : $this->makeEvent( $event );

        $this->getDispatcher( $event->getType() )->dispatch( $event->getName(), $params );
    }

    public function addDispatcher( $id, EventDispatcherInterface $dispatcher )
    {
        $this->dispatchers[$id] = $dispatcher;

        return $this;
    }

    public function getDispatcher( $id )
    {
        return $this->dispatchers[$id];
    }
}
