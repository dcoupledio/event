<?php namespace Decoupled\Core\Event;

interface EventDelegatorInterface{

    public function when( $event );

    public function dispatch( $event, array $params = [] );

    public function addDispatcher( $id, EventDispatcherInterface $dispatcher );

    public function getDispatcher( $id );
}
