<?php namespace  Decoupled\Core\Event;

use Symfony\Component\EventDispatcher\EventDispatcher as SymfonyDispatcher;

class EventDispatcher implements EventDispatcherInterface{

    public function __construct()
    {
        $this->dispatcher = new SymfonyDispatcher();
    }

    /**
    * Dispatches given event
    **/

    public function dispatch( $eventName, array $params = [] )
    {
        $e = new DispatchedEvent();

        $e->setParameters( $params );

        $this->dispatcher->dispatch( $eventName, $e );
    }

    /**
    * @return Decoupled\Core\Application\Extension\Event\EventListenerInterface
    **/

    public function addListener( EventListenerInterface $listener )
    {   
        $event = $listener->getEvent()->getName();

        $this->dispatcher->addListener( $event, [ $listener, 'invoke' ] );
    }
}