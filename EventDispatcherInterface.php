<?php namespace Decoupled\Core\Event;

interface EventDispatcherInterface{

    /**
    * Dispatches given event
    **/

    public function dispatch( $eventName, array $params = [] );

    /**
    * @return Decoupled\Core\Application\Extension\Event\EventListenerInterface
    **/

    public function addListener( EventListenerInterface $event );
}