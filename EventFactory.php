<?php namespace Decoupled\Core\Event;

class EventFactory implements EventFactoryInterface{

    public function make( $eventName )
    {
        $event = new Event();

        $event->setName( $eventName )->setType( Event::DEFAULT_TYPE );

        return $event;
    }
}