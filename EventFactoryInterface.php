<?php namespace Decoupled\Core\Event;

interface EventFactoryInterface{

    public function make( $event );
}