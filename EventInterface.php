<?php namespace Decoupled\Core\Event;

interface EventInterface{

    public function getName();

    public function getType();
}