<?php namespace Decoupled\Core\Event;

use Decoupled\Core\Action\ActionFactoryInterface;

class EventListener implements EventListenerInterface{

    protected $event;

    protected $action;

    protected $actionFactory;

    public function setEvent( EventInterface $event )
    {
        $this->event = $event;

        return $this;
    }

    public function getEvent()
    {
        return $this->event;
    }

    public function uses( $callback )
    {
        $this->action = $this->getActionFactory()->make( $callback );

        return $this;
    }

    public function setActionFactory( ActionFactoryInterface $factory )
    {
        $this->actionFactory = $factory;

        return $this;
    }

    public function getActionFactory()
    {
        return $this->actionFactory;
    }

    public function getAction()
    {
        return $this->action;
    }

    public function invoke( DispatchedEventInterface $event )
    {   
        $event->setEvent( $this->getEvent() );

        $params = array_merge( $event->getParameters(), [ 'event' => $event] );

        $action = $this->getAction();

        return $action( $params );
    }
}