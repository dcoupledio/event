<?php namespace Decoupled\Core\Event;

use Decoupled\Core\Action\ActionFactoryInterface;

class EventListenerFactory implements EventListenerFactoryInterface{

    protected $actionFactory;

    public function make( EventInterface $event )
    {
        $listener = new EventListener();

        $listener->setEvent( $event );

        if( $factory = $this->getActionFactory() )
        {
            $listener->setActionFactory( $factory );
        }

        return $listener;
    }

    public function setActionFactory( ActionFactoryInterface $factory )
    {
        $this->actionFactory = $factory;

        return $this;
    }

    public function getActionFactory()
    {
        return $this->actionFactory;
    }
}