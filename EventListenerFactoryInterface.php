<?php namespace Decoupled\Core\Event;

interface EventListenerFactoryInterface{

    public function make( EventInterface $event );
}