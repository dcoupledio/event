<?php namespace Decoupled\Core\Event;

interface EventListenerInterface{

    public function setEvent( EventInterface $event );

    public function getEvent();

    public function uses( $callback );

    public function invoke( DispatchedEventInterface $event );
}